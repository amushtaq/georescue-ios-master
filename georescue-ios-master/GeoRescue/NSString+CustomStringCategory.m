//
//  NSString+CustomStringCategory.m
//  SocialGeoApp
//
//  Created by Asim Mushtaq on 08/01/2015.
//  Copyright (c) 2015 GeoSpat. All rights reserved.
//

#import "NSString+CustomStringCategory.h"

@implementation NSString (CustomStringCategory)

-(NSString*)pureString:(NSString*)strLocal {
    
    NSString *strReturn = @"";
    
    if (strLocal == nil || strLocal == NULL || [strLocal isEqual:NULL] || strLocal == (id)[NSNull null] || strLocal.length == 0 || [strLocal isEqualToString:@"null"])
    {
        strReturn = @"";
    }
    else
    {
        strReturn = strLocal;
    }
    
    return strReturn;
}

@end

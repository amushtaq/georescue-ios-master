//
//  siginInViewController.h
//  GeoRescue
//
//  Created by faridullah on 20/05/2015.
//  Copyright (c) 2015 magmasys. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASIHTTPRequest.h"

@interface siginInViewController : UIViewController<ASIHTTPRequestDelegate>
@property (strong, nonatomic) IBOutlet UITextField *emailText;
@property (strong, nonatomic) IBOutlet UITextField *pwdTextFld;

- (IBAction)siginInBtnAction:(id)sender;
- (IBAction)facebookBtn:(id)sender;
- (IBAction)googlePlusBtn:(id)sender;
@end

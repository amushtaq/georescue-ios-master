//
//  personalInfoViewController.m
//  GeoRescue
//
//  Created by faridullah on 21/05/2015.
//  Copyright (c) 2015 magmasys. All rights reserved.
//

#import "personalInfoViewController.h"
#import "pInfoProceedViewController.h"
@interface personalInfoViewController ()

@end

@implementation personalInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    PickerDataArray= [NSMutableArray arrayWithObjects:
                       @"A-",@"A+",@"B-",@"B+",@"AB-",@"AB+",@"O-",@"O+",nil];
    UITapGestureRecognizer* gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(valueLabelAction)];
    // if labelView is not set userInteractionEnabled, you must do so
    [self.valueLabel setUserInteractionEnabled:YES];
    [self.valueLabel addGestureRecognizer:gesture];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)valueLabelAction{
    [self.pickView setHidden:NO];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
// The number of columns of data
- (int)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// The number of rows of data
- (int)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return PickerDataArray.count;
}

// The data to return for the row and component (column) that's being passed in
- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return PickerDataArray[row];
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    // This method is triggered whenever the user makes a change to the picker selection.
    // The parameter named row and component represents what was selected.
    self.valueLabel.text=[PickerDataArray objectAtIndex:row];
    self.pickView.hidden=YES;
}

- (IBAction)skipOrProceedBtn:(id)sender {
    pInfoProceedViewController *obj1Vc = [[pInfoProceedViewController alloc]initWithNibName:@"pInfoProceedViewController" bundle:nil];
    [self.navigationController pushViewController:obj1Vc animated:YES];
    
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
   
}
@end

//
//  pInfoProceedViewController.h
//  GeoRescue
//
//  Created by faridullah on 21/05/2015.
//  Copyright (c) 2015 magmasys. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface pInfoProceedViewController : UIViewController
- (IBAction)proceedBtnAction:(id)sender;

@end

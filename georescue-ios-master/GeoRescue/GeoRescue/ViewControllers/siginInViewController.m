//
//  siginInViewController.m
//  GeoRescue
//
//  Created by faridullah on 20/05/2015.
//  Copyright (c) 2015 magmasys. All rights reserved.
//

#import "siginInViewController.h"
#import "ASIHTTPRequest.h"
#import "JSON.h"
#import "UserDefaultsHelper.h"
#import "mapViewController.h"
#import "ASIFormDataRequest.h"
#import "personalInfoViewController.h"
@interface siginInViewController ()

@end

@implementation siginInViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)callSignInService{
    NSString *gURL = [NSString stringWithFormat:@"http://api.georescue.services/login"];
    ASIFormDataRequest* request = [[ASIFormDataRequest alloc] initWithURL:[NSURL URLWithString:gURL]];
    [request setDelegate:self];
    [request addRequestHeader:@"Content-Type" value:@"application/json"];
    [request setPostValue:@"true" forKey:@"createNew"];
    [request setPostValue:self.emailText.text forKey:@"email"];
    [request setPostValue:self.pwdTextFld.text forKey:@"password"];
    [request startAsynchronous];
}
#pragma mark - ASIHTTPRequest Delegate

- (void)requestFinished:(ASIHTTPRequest *)request {
    
    NSString *responseString = [request responseString];
    
    NSDictionary* respDict = [[responseString JSONValue] objectAtIndex:0];
    NSString* strUserID = [respDict objectForKey:@"result"];

    if ([strUserID isEqualToString:@"true"])
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Sign In Successful" message:nil delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alertView show];
        
        
        mapViewController *obj1Vc = [[mapViewController alloc]initWithNibName:@"mapViewController" bundle:nil];
        [self.navigationController pushViewController:obj1Vc animated:YES];
    }
   else
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Info" message:responseString delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alertView show];
    }
    
}

- (void)requestFailed:(ASIHTTPRequest *)request {
  
    NSError * error = [request error];
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Request Failed. Try again later." message:[error localizedDescription] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    
    [alertView show];
}

- (IBAction)siginInBtnAction:(id)sender {
    personalInfoViewController *obj1Vc = [[personalInfoViewController alloc]initWithNibName:@"personalInfoViewController" bundle:nil];
    [self.navigationController pushViewController:obj1Vc animated:YES];
}

- (IBAction)facebookBtn:(id)sender {
}

- (IBAction)googlePlusBtn:(id)sender {
}

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [[self view] endEditing:YES];
}
@end

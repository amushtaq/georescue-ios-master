//
//  offLineMapViewController.h
//  GeoRescue
//
//  Created by faridullah on 25/05/2015.
//  Copyright (c) 2015 magmasys. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Mapbox.h"
@interface offLineMapViewController : UIViewController<RMMapViewDelegate>

@end

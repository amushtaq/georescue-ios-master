//
//  personalInfoViewController.h
//  GeoRescue
//
//  Created by faridullah on 21/05/2015.
//  Copyright (c) 2015 magmasys. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface personalInfoViewController : UIViewController<UIPickerViewDataSource, UIPickerViewDelegate>
{
    NSMutableArray *PickerDataArray;
}
- (IBAction)skipOrProceedBtn:(id)sender;
@property (strong, nonatomic) IBOutlet UIPickerView *pickView;
@property (strong, nonatomic) IBOutlet UILabel *valueLabel;

@end

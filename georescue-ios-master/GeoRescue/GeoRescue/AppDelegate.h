//
//  AppDelegate.h
//  GeoRescue
//
//  Created by faridullah on 20/05/2015.
//  Copyright (c) 2015 magmasys. All rights reserved.
//

#import <UIKit/UIKit.h>
@class siginInViewController;
@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) siginInViewController *viewController;
@property(strong,nonatomic) UINavigationController *navBar;
@end


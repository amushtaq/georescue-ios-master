//
//  mapViewController.h
//  GeoRescue
//
//  Created by faridullah on 20/05/2015.
//  Copyright (c) 2015 magmasys. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface mapViewController : UIViewController<MKMapViewDelegate>

@property (strong, nonatomic) IBOutlet MKMapView *mapView;

@end

//
//  UserDefaultsHelper.h
//  Pet Finder
//
//  Created by Asim Mushtaq on 9/23/13.
//  Copyright (c) 2013 GeoSpat. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserDefaultsHelper : NSObject

+ (void)setFirstProfileAdded:(BOOL)num;
+ (BOOL)getFirstProfileAdded;

+ (void)setSignUpCompleted:(BOOL)num;
+ (BOOL)getSignUpCompleted;

+ (void)setUserName:(NSString*)name;
+ (NSString*)getUserName;

+ (void)setEmailAddress:(NSString*)email;
+ (NSString*)getEmailAddress;

+ (void)setMobileNumber:(NSString*)num;
+ (NSString*)getMobileNumber;

+ (void)setDeviceToken:(NSString*)token;
+ (NSString*)getDeviceToken;

+ (void)setHomeLatitude:(double)lat;
+ (double)getHomeLatitude;

+ (void)setHomeLongtude:(double)lon;
+ (double)getHomeLongitude;

+ (void)setUserId:(NSString*)strID;
+ (NSString*)getUserId;

+ (void)setUserImage:(NSString*)strImage;
+ (NSString*)getUserImage;

+ (void)setPassword:(NSString*)strPassword;
+ (NSString*)getPassword;

+ (void)setRememberMe: (NSString*)ans;
+ (NSString*)getRememberMe;

+ (void)setFirstScreen:(NSString*)ans;
+ (NSString*)getFirstScreen;

@end

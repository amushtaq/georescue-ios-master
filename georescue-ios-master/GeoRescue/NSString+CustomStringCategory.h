//
//  NSString+CustomStringCategory.h
//  SocialGeoApp
//
//  Created by Asim Mushtaq on 08/01/2015.
//  Copyright (c) 2015 GeoSpat. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (CustomStringCategory)

-(NSString*)pureString:(NSString*)strLocal;

@end

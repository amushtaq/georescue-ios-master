//
//  Constants.h
//  Pet Finder
//
//  Created by Asim Mushtaq on 9/23/13.
//  Copyright (c) 2013 GeoSpat. All rights reserved.
//

#ifndef Pet_Finder_Constants_h
#define Pet_Finder_Constants_h

#define kSettServerbaseURl          @"http://magmasysdev.com/Petfinder1.3/"
#define kSettImageBaseURL           @"http://magmasysdev.com"
//#define kSettUserID                 @"test_iOS_UUID1"
#define kSettDeviceToken            @"deviceTokenForPushNotifications"

// USER RELATED //
#define kSettUserhaveSignedUp       @"userHaveSignedUp"
#define kSettUserName               @"SavedUserName"
#define kSettEmailAddr              @"userEmailAddress"
#define kSettMobileNum              @"userContactNumber"
#define kSettFirstPetAdded          @"profileAddedForFirstPet"
#define kSettHomeLatitude           @"userzHomeLatitude"
#define kSettHomeLongitude          @"userzHomeLongitude"

#define kSettClientIdGooglePlus     @"1013661935050-pqgh82n60mekj7colpi3s78mrui1amjv.apps.googleusercontent.com"
#define API_KEY_FOR_Street_View     @"AIzaSyCV5R6urk3dEXNq29T87wwzNnE0tVHTnjs"


// Testing Database Related //=== Arslan ===

#define kServerType @"_testing" //--for testing

//#define kServerType @"" //--for Live

#endif
